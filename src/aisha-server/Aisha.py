__author__ = 'mahesmohan'

import sys
from settings import routes, config
from settings.altEngine import PROJECT_ROOT
from bottle import Bottle, debug, default_app

debug(config.debug)

sys.path.insert(0, PROJECT_ROOT)

if __name__ == '__main__':
    Aisha = Bottle()
    routes.set(Aisha)
    Aisha.run(host='0.0.0.0', port=8080, reloader=True)
else:
    AishaApp = default_app()
    routes.set(AishaApp)
