var conf= {
	x: 0,
	y: 0,
	textBlur: 10,
	closeOnESC: false,
	crsrBlinkMode: true,
	initHandler: initHandler,
	handler: handler
}

var baseUrl = "http://localhost:8080/"

function json(string) {
	return eval('('+string+')');
}

var ajaxSettings = {
	type: 'POST',
	contentType: 'application/json',
	async: false
}
$.ajaxSetup(ajaxSettings);


var term = new Terminal(conf);

function termOpen(element) {
	term.open();
	element.focus();
}

function initHandler() {
	term.write("Initializing AIShA...");
	term.newLine();
	$.ajax({
		url: baseUrl,
	}).success(function(data){
		term.write(data.message);
		term.newLine();
		term.prompt();
	}).error(function(){
		term.write('Could not establish connection.');
	});
}

function handler() {
	if(term.lineBuffer !='') {
		var reqData = JSON.stringify({"command":term.lineBuffer});
		console.log(reqData);
		term.newLine();
		$.ajax({
			url: baseUrl+"command/",
			data: reqData
		}).success(function(data){
			term.write(data.message);
			term.newLine();
			term.prompt();
		}).error(function(){
			term.write('Connection lost.');
		});
	}
}

function commandHandlerCallback() {
	if(term.socket.success) {
		var data = json(term.socket.responseText);
		term.write(data.message);
		term.newLine();
		term.prompt();
	} else {
		term.write('Could not establish connection.');
	}
}