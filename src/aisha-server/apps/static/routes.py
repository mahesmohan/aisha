__author__ = 'mahesmohan'
from bottle import Bottle, static_file
from settings.altEngine import PROJECT_ROOT

jsRoutes = Bottle()
cssRoutes = Bottle()

def jsFileRequest(filename):
	return static_file(filename, PROJECT_ROOT+'/apps/static/js')

def cssFileRequest(filename):
	return static_file(filename, PROJECT_ROOT+'/apps/static/css')

jsRoutes.route('/<filename>', 'GET', jsFileRequest)
cssRoutes.route('/<filename>', 'GET', cssFileRequest)
