__author__ = 'mahesmohan'
from settings import config
from bottle import hook, response, request, template, TEMPLATE_PATH
from settings.altEngine import enable_cors
from settings.altEngine import PROJECT_ROOT

def htmlIndex():
	TEMPLATE_PATH.insert(0, PROJECT_ROOT+'/apps/main/templates/')
	print TEMPLATE_PATH
	return template('htmlIndex')

def index():
    responseData = {}
    msg = []
    msg.append('Connection established.')
    msg.append('Running v'+ str(config.version) + 'd' if config.debug else '')
    responseData['message'] = msg
    enable_cors(response)
    return responseData

def command():
	enable_cors(response)
	obj = request.json
	print request
	responseData = {}
	try:
		command = str(obj['command']).split(' ')
	except:
		command = "No object recieved"
	responseData['message'] = command
	return responseData

def option():
	enable_cors(response)
	return response