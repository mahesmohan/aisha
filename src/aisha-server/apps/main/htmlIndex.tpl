<html>
	<head>
		<script src="js/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="js/termlib.js"></script>
		<script type="text/javascript" src="js/boot.js"></script>
		<link rel="stylesheet" type="text/css" href="css/style.css" />

	</head>
	<body>
		<div id="termDiv">
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				termOpen($('#termDiv'));
			});
		</script>
	</body>
</html>