__author__ = 'mahesmohan'
from apps.main.views import *
from apps.static.routes import jsRoutes, cssRoutes

def set(app):
    app.route('/', 'POST', index)
    app.route('/', ['GET'], htmlIndex)
    app.route('/command/', ['POST'], command)
    app.mount('/js/', jsRoutes)
    app.mount('/css/', cssRoutes)
