__author__ = 'mahesmohan'
import os
PROJECT_ROOT = os.path.realpath(os.path.dirname(os.path.dirname(__file__)))

def enable_cors(response):
    response.headers['Access-Control-Allow-Origin'] = 'http://localhost'
    response.headers['Access-Control-Allow-Headers'] = '*'
